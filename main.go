package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello from our GitLab Meetup")

	fmt.Println("");

	fmt.Print(GetTanuki(true))

	fmt.Println("");
	fmt.Println("Join us at https://www.everyonecancontribute.com")
}
